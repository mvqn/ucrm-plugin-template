<?php
/** @noinspection PhpUnused */
/** @noinspection PhpUndefinedClassInspection */
/** @noinspection PhpUnusedPrivateMethodInspection */
/** @noinspection PhpUnusedLocalVariableInspection */
/** @noinspection PhpDeprecationInspection */
declare(strict_types=1);
require_once __DIR__."/src/server/vendor/autoload.php";

use MVQN\Robo\Task\Sftp\Exceptions\ConfigurationMissingException;
use MVQN\Robo\Task\Sftp\Exceptions\ConfigurationParsingException;
use MVQN\Robo\Task\Sftp\Exceptions\OptionMissingException;
use MVQN\SFTP\Exceptions\AuthenticationException;
use MVQN\SFTP\Exceptions\InitializationException;
use MVQN\SFTP\Exceptions\LocalStreamException;
use MVQN\SFTP\Exceptions\MissingExtensionException;
use MVQN\SFTP\Exceptions\RemoteConnectionException;
use MVQN\SFTP\Exceptions\RemoteStreamException;
use Robo\Tasks;

/**
 * Class RoboFile
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */
class RoboFile extends Tasks
{
    private const ENV_FILE_PATH         = __DIR__ . DIRECTORY_SEPARATOR . "src" . DIRECTORY_SEPARATOR . ".env";
    private const REMOTE_PLUGIN_PATH    = "/home/unms/data/ucrm/ucrm/data/plugins";

    use MVQN\Robo\Task\Sftp\Tasks;
    use MVQN\Robo\Task\Packer\Tasks;

    #region HELPERS

    /**
     * Builds the correct path string, given one or more path parts.
     *
     * @noinspection PhpDocSignatureInspection
     * @param string ...$parts The parts of a path to be joined.
     * @return string Returns a string with all path parts joined by the current operating system's directory separator.
     */
    private function path(string ...$parts): string
    {
        return implode(DIRECTORY_SEPARATOR, $parts);
    }

    #endregion

    #region METADATA

    /**
     * Gets the current Plugin's manifest, as an associative array.
     *
     * @return array|null Returns the Plugin's manifest, or NULL if there is any issue finding/parsing the file.
     */
    private function getManifest(): ?array
    {
        if(!($path = realpath($this->path(__DIR__, "src", "manifest.json"))))
            return null;

        $manifest = json_decode(file_get_contents($path), true);
        return json_last_error() === JSON_ERROR_NONE ? $manifest : null;
    }

    /**
     * Gets the current Plugin name.
     *
     * @return string|null Returns the Plugin's name, as it appears in the manifest file.
     */
    private function getPluginName(): ?string
    {
        return $this->getManifest()["information"]["name"] ?? null;
    }

    /**
     * Gets the current Plugin version.
     *
     * @return string|null Returns the Plugin's version, as it appears in the manifest file.
     */
    private function getPluginVersion(): ?string
    {
        return $this->getManifest()["information"]["version"] ?? null;
    }

    #endregion

    #region VALIDATION

    /**
     * @return bool Returns TRUE if the Plugin's folder and file structure is valid, otherwise FALSE.
     */
    private function isValid(): bool
    {
        // TODO: Create some validation logic!
        return true;
    }

    #endregion

    /**
     * @throws AuthenticationException
     * @throws ConfigurationMissingException
     * @throws ConfigurationParsingException
     * @throws InitializationException
     * @throws LocalStreamException
     * @throws MissingExtensionException
     * @throws OptionMissingException
     * @throws RemoteConnectionException
     * @throws RemoteStreamException
     */
    public function initSync()
    {
        $plugin = $this->getPluginName();

        $remoteBase = self::REMOTE_PLUGIN_PATH."/$plugin";
        $localBase  = __DIR__."\\src";

        $this->taskSftpGet()

            ->loadConfiguration()

            ->funcConfiguration(
                function(array $current)
                {
                    if ($current["host"] === "" || $current["port"] === "" ||
                        $current["user"] === "" || $current["pass"] === "")
                        $current = $this->askSftpConfiguration(__DIR__, "sftp.config.json");

                    return $current;
                }
            )

            //->setRemoteBase()
            //->setLocalBase()

            ->maps([
                "$remoteBase/ucrm.json"                 => "$localBase\\ucrm.json",
                "$remoteBase/data/config.json"          => "$localBase\\data\\config.json",
                "$remoteBase/data/plugin.log"           => "$localBase\\data\\plugin.log",
                "$remoteBase/data/plugin.db"            => "$localBase\\data\\plugin.db",
                "$remoteBase/server/App/Settings.php"   => "$localBase\\server\\App\\Settings.php",
            ])

            ->run();


    }

    #region CLIENT

    /**
     * Serves the client-size application using the development web server.
     *
     * *NOTE: If the client-side application uses any server-side requests, the server-side must be running also.*
     *
     * @see https://cli.vuejs.org/guide/cli-service.html
     * @return bool Returns TRUE, when the process has executed successfully, otherwise FALSE.
     */
    public function clientRun(): bool
    {
        return $this->taskExec("cd src/client/ && yarn serve")->run()->wasSuccessful();
    }

    /**
     * Updates all client-side packages, via {@link https://yarnpkg.com/ Yarn }.
     *
     * @return bool Returns TRUE, when the process has executed successfully, otherwise FALSE.
     */
    public function clientUpdate(): bool
    {
        return $this->taskExec("cd src/client/ && yarn upgrade")->run()->wasSuccessful();
    }

    /**
     * Builds the stand-alone, client-side application using vue-cli-service (and webpack).
     *
     * @see https://cli.vuejs.org/guide/cli-service.html
     * @see https://webpack.js.org/
     * @return bool Returns TRUE, when the process has executed successfully, otherwise FALSE.
     */
    public function clientBuild(): bool
    {
        return $this->taskExec("cd src/client/ && yarn build")->run()->wasSuccessful();
    }

    /**
     * Performs a simultaneous update and build, using the individual {@link clientUpdate} and {@link clientBuild}
     * methods.
     *
     * @return bool Returns TRUE, when both processes have executed successfully, otherwise FALSE.
     */
    public function clientDeploy(): bool
    {
        return $this->clientUpdate() && $this->clientBuild();
    }

    #endregion

    #region BUNDLE

    /**
     * Bundles this project into the correct UCRM Plugin format.
     *
     * **IMPORTANT:** *This procedure relies entirely upon the assumption that this project's root folder structure is
     * identical to that of the {@link https://gitlab.com/mvqn/ucrm-plugin-template ucrm-plugin-template}.  Any other
     * variation will almost certainly break the process and can potentially cause other unexpected issues.*
     *
     * @param bool $optimize When TRUE, optimizations will be used during bundling. Defaults to FALSE.
     */
    public function bundle(bool $optimize = false): void
    {
        // IF the plugin is NOT valid, THEN simply die!
        if(!$this->isValid())
            die("This Plugin is not valid, exiting!");

        // Setup the Packer options.
        $options = [
            "folder" => __DIR__."/src/",
            "ignore" => __DIR__."/src/.zipignore",
            "output" => [
                "name" => $this->getPluginName(), //."_".$this->getPluginVersion(),
                "path" => __DIR__,
            ],
        ];

        // Get the paths to the original and the revised "composer.json" and "composer.lock" files for fix-ups.
        $originalPath = __DIR__.DIRECTORY_SEPARATOR."composer";
        $revisionPath = __DIR__.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."composer";

        // Use our pre-made Packer Robo Task...
        $this->taskPackerBundle($options)

            // Run any functionality immediately before bundling...
            ->before(
                function() use ($originalPath, $revisionPath, $optimize)
                {
                    // NOTE: These next two blocks are necessary, because the folder structure of the bundle differs
                    // from that of the project!

                    // Copy the original composer.* files into the "src/" folder.
                    echo "Copying composer.json to src\\...\n";
                    echo exec("copy $originalPath.json $revisionPath.json") . "\n";
                    echo "Copying composer.lock to src\\...\n";
                    echo exec("copy $originalPath.lock $revisionPath.lock") . "\n";

                    // Fix any relative paths in the composer.json file.
                    echo "Fixing composer.json...\n";
                    $contents = file_get_contents("$revisionPath.json");
                    $contents = str_replace("src/", "./", $contents);
                    file_put_contents("$revisionPath.json", $contents);

                    // IF we are requesting an optimized bundle...
                    if($optimize)
                    {
                        // NOTE: We do this to prevent having to re-download everything after the bundling process.

                        // ...THEN remove any previous backups of the vendor folder.
                        if(file_exists("src\\server\\vendor_bak\\"))
                        {
                            echo "Removing old vendor backup...\n";
                            echo exec("CD src\\server\\ && RMDIR /S /Q vendor_bak");
                        }

                        // ...AND make a backup of the entire vendor folder.
                        if(file_exists("src\\server\\vendor\\"))
                        {
                            echo "Creating new vendor backup...\n";
                            /** @noinspection SpellCheckingInspection */
                            echo exec("CD src\\server\\ && ROBOCOPY /S /E vendor vendor_bak");
                        }

                        // Run a composer update without development dependencies!
                        // NOTE: This will automatically generate the new autoload class maps.
                        echo "Updating composer non-development dependencies...\n";
                        echo exec("cd src\\ && composer --no-interaction --verbose --no-dev update");
                    }
                    else
                    {
                        // ...OTHERWISE, assume we already have the dependencies the way we want.

                        // IF the vendor folder already exists...
                        if(file_exists("src\\server\\vendor\\"))
                        {
                            // ...THEN simply regenerate the autoload class maps.
                            echo "Updating composer class maps...\n";
                            echo exec("cd src\\ && composer --no-interaction --verbose dump-autoload");
                        }
                        else
                        {
                            // OTHERWISE, run a composer update with development dependencies.
                            // NOTE: This will automatically generate the new autoload class maps.
                            echo "Updating composer dependencies...\n";
                            echo exec("cd src\\ && composer --no-interaction --verbose update");
                        }
                    }
                }
            )

            // Run any functionality immediately after bundling...
            ->after(
                function() use ($originalPath, $revisionPath, $optimize)
                {
                    // NOTE: Now we undo everything we did before the bundling process.

                    // Remove the "fixed" composer.* files.
                    echo "Removing revised composer.json...\n";
                    echo exec("del /F $revisionPath.json");
                    echo "Removing revised composer.lock...\n";
                    echo exec("del /F $revisionPath.lock");

                    // IF we requested an optimized bundle...
                    if($optimize)
                    {
                        // ...THEN remove the current vendor folder, ONLY if a backup folder exists.
                        if(file_exists("src\\server\\vendor\\") && file_exists("src\\server\\vendor_bak\\"))
                        {
                            echo "Removing optimized vendor folder...\n";
                            echo exec("CD src\\server\\ && RMDIR /S /Q vendor");
                        }

                        // ...AND delete the backup vendor folder.
                        if(file_exists("src\\server\\vendor_bak\\"))
                        {
                            echo "Restoring original vendor folder from backup...\n";
                            echo exec("CD src\\server\\ && REN vendor_bak vendor");
                        }
                    }

                    // Finally, regenerate the autoload class maps back using the previous paths.
                    echo "Updating composer class maps...\n";
                    echo exec("composer --no-interaction --verbose dump-autoload");
                }
            )

            // Execute the Packer Robo task!
            ->run();

    }

    #endregion

    #region HOOKS

    /**
     * Simulates the Plugin being installed by executing the "hook_install.php" script.
     *
     * @see https://github.com/Ubiquiti-App/UCRM-plugins/blob/master/docs/file-structure.md#hook_php-files
     */
    public function hookInstall(): void
    {
        $this->say("Executing the Plugin's install hook...");
        $this->_exec("php ./src/hook_install.php");
    }

    /**
     * Simulates the Plugin being removed by executing the "hook_remove.php" script.
     *
     * @see https://github.com/Ubiquiti-App/UCRM-plugins/blob/master/docs/file-structure.md#hook_php-files
     */
    public function hookRemove(): void
    {
        $this->say("Executing the Plugin's remove hook...");
        $this->_exec("php ./src/hook_remove.php");
    }

    /**
     * Simulates the Plugin being updated by executing the "hook_update.php" script.
     *
     * @see https://github.com/Ubiquiti-App/UCRM-plugins/blob/master/docs/file-structure.md#hook_php-files
     */
    public function hookUpdate(): void
    {
        $this->say("Executing the Plugin's update hook...");
        $this->_exec("php ./src/hook_update.php");
    }

    /**
     * Simulates the Plugin being enabled by executing the "hook_enable.php" script.
     *
     * @see https://github.com/Ubiquiti-App/UCRM-plugins/blob/master/docs/file-structure.md#hook_php-files
     */
    public function hookEnable(): void
    {
        $this->say("Executing the Plugin's enable hook...");
        $this->_exec("php ./src/hook_enable.php");
    }

    /**
     * Simulates the Plugin being disabled by executing the "hook_disable.php" script.
     *
     * @see https://github.com/Ubiquiti-App/UCRM-plugins/blob/master/docs/file-structure.md#hook_php-files
     */
    public function hookDisable(): void
    {
        $this->say("Executing the Plugin's disable hook...");
        $this->_exec("php ./src/hook_disable.php");
    }

    /**
     * Simulates the Plugin being configured by executing the "hook_configure.php" script.
     *
     * @see https://github.com/Ubiquiti-App/UCRM-plugins/blob/master/docs/file-structure.md#hook_php-files
     */
    public function hookConfigure(): void
    {
        $this->say("Executing the Plugin's configure hook...");
        $this->_exec("php ./src/hook_configure.php");
    }

    #endregion

    #region CONSOLE

    /**
     * Prompts the user for a required answer to a question.
     *
     * @param string $question A question for which to prompt the user.
     * @param callable|null $validator An optional function to validate the answer.
     *
     * @return string Returns the answer to the question.
     */
    private function askRequired(string $question, callable $validator = null): string
    {
        return $this->askDefaultRequired($question, null, $validator);
    }

    /**
     * Prompts the user for a required answer to a question.
     *
     * @param string $question A question for which to prompt the user.
     * @param array $allowed An optional array of possible answers.
     *
     * @return string Returns the answer to the question.
     */
    private function askRequiredAllowed(string $question, array $allowed = []): string
    {
        return $this->askDefaultRequiredAllowed($question, null, $allowed);
    }

    /**
     * Prompts the user for a required answer to a question.
     *
     * @param string $question A question for which to prompt the user.
     * @param string|null $pattern An optional RegEx pattern to for which to match the answer.
     *
     * @param string|null $alternate
     * @return string Returns the answer to the question.
     */
    private function askRequiredMatch(string $question, string $pattern = null, string $alternate = null): string
    {
        return $this->askDefaultRequiredMatch($question, null, $pattern, $alternate);
    }

    /**
     * Prompts the user for a required answer to a question.
     *
     * @param string $question A question for which to prompt the user.
     * @param string|null $default An optional default value to be used as the answer.
     * @param callable|null $validator An optional function to validate the answer.
     *
     * @return string Returns the answer to the question.
     */
    private function askDefaultRequired(string $question, string $default = null, callable $validator = null): string
    {
        $answer     = "";
        $default    = $default ?? "";
        $validator  = $validator ?? function() { return true; };

        while ((!$answer && $answer !== "0") || !$validator($answer))
        {
            // NULL if empty or whitespace!
            $answer = $default ? $this->askDefault($question, $default) : $this->ask($question);

            if(!$answer)
                $this->say("An answer is required!");
        }

        return $answer;
    }

    /**
     * Prompts the user for a required answer to a question.
     *
     * @param string $question A question for which to prompt the user.
     * @param string|null $default An optional default value to be used as the answer.
     * @param array $allowed An optional array of possible answers.
     *
     * @return string Returns the answer to the question.
     */
    private function askDefaultRequiredAllowed(string $question, string $default = null, array $allowed = []): string
    {
        return $this->askDefaultRequired($question, $default,

            function(string $answer) use ($allowed)
            {
                $valid = count($allowed) === 0 || in_array($answer, $allowed);

                if(!$valid)
                    $this->say("Answers must be one of the following: " . implode(" | ", $allowed));

                return $valid;
            }
        );
    }

    /**
     * Prompts the user for a required answer to a question.
     *
     * @param string $question A question for which to prompt the user.
     * @param string|null $default An optional default value to be used as the answer.
     * @param string|null $pattern An optional RegEx pattern to for which to match the answer.
     *
     * @param string|null $alternate
     * @return string Returns the answer to the question.
     */
    private function askDefaultRequiredMatch(string $question, string $default = null, string $pattern = null, string $alternate = null): string
    {
        return $this->askDefaultRequired($question, $default,

            function(string $answer) use ($pattern, $alternate)
            {
                $valid = $pattern === null || preg_match($pattern, $answer);

                if(!$valid)
                    $this->say($alternate !== null ? $alternate : "Answers must match pattern: " . $pattern);

                return $valid;
            }
        );
    }

    #endregion

    #region COMPOSER


    private function replaceJsonString(string &$original, string $key, string $value, string $replacement): bool
    {
        $count = 0;

        $value = str_replace("/", "\/", $value);

        $original = preg_replace(
            '/("'.$key.'"\s*:\s*")('.$value.')(")/',
            '${1}'.$replacement.'${3}',
            $original,
            -1,
            $count
        );

        return $count > 0;
    }

    private function replaceJsonStringArray(string &$original, string $key, array $value, array $replacement): bool
    {
        $count = 0;

        $value = str_replace("/", "\/", $value);

        $original = preg_replace(
            '/("'.$key.'"\s*:\s*\[)(\s*"'.implode('"\s*,\s*"', $value).'"\s*)(\])/',
            '${1} "'.implode('", "', $replacement).'" ${3}',
            $original,
            -1,
            $count
        );

        return $count > 0;
    }




    public function composerPostCreateProjectCmd(): void
    {
        $options = [
            "plugin" => [
                "vendor" => "mvqn",
                "project" => "ucrm-plugin-template",
                "name" => "Plugin Template",
                "description" => "A project template for developing UCRM Plugins.",
                "version" => "1.0.0",
                "license" => "MIT",
                "url" => "https://gitlab.com/mvqn/ucrm-plugin-template",
            ],
            "author" => [
                "name" => "Ryan Spaeth",
                "email" => "rspaeth@mvqn.net",
            ],
            "hooks" => [
                "configure" => true,
                "disable"   => true,
                "enable"    => true,
                "install"   => true,
                "remove"    => true,
                "update"    => true,
            ],
            "server" => [

            ],
            "client" => [

            ]
        ];

        $this->io()->title("Information");

        // NOTE: Composer 2.0 will enforce: "/^[a-z0-9]([_.-]?[a-z0-9]+)*\/[a-z0-9]([_.-]?[a-z0-9]+)*$/"

        $options["plugin"]["vendor"] = $this->askDefaultRequiredMatch(
            "Plugin Vendor     ", $options["plugin"]["vendor"],
            "/^[A-Za-z_-]+$/"
        );
        $options["plugin"]["project"] = $this->askDefaultRequiredMatch(
            "Plugin Project    ", $options["plugin"]["project"],
            "/^[A-Za-z_-]+$/"
        );

        $options["plugin"]["name"] = $this->askDefaultRequiredMatch(
            "Plugin Label      ", $options["plugin"]["name"],
            "/^.+$/"
        );
        $options["plugin"]["description"] = $this->askDefaultRequiredMatch(
            "Plugin Description", $options["plugin"]["description"],
            "/^.+$/"
        );

        $options["plugin"]["version"] = $this->askDefaultRequiredMatch(
            "Plugin Version    ", $options["plugin"]["version"],
            "/^(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)\.(0|[1-9][0-9]*)(?:-((?:0|[1-9][0-9]*|[0-9]*[a-zA-Z-][0-9a-zA-Z-]*)".
            "(?:\.(?:0|[1-9][0-9]*|[0-9]*[a-zA-Z-][0-9a-zA-Z-]*))*))?(?:\+([0-9a-zA-Z-]+(?:\.[0-9a-zA-Z-]+)*))?$/",
            "Plugin Version must be in a semantic version format!"
        );

        $options["plugin"]["license"] = $this->askDefaultRequiredMatch(
            "Plugin License    ", $options["plugin"]["license"],
            "/^.+$/"
        );

        $options["plugin"]["url"] = $this->askDefaultRequiredMatch(
            "Plugin URL        ", $options["plugin"]["url"],
            //"/^.+$/"
            "/^https?:\/\/(www\.)?[-a-zA-Z0-9@:%._\+~#=]{1,256}\.[a-zA-Z0-9()]{1,6}\b([-a-zA-Z0-9()@:%_\+.~#?&\/=]*)/"
        );



        $options["author"]["name"] = $this->askDefaultRequiredMatch(
            "Author Name       ", $options["author"]["name"],
            //"/^[a-zA-Z ,.]+$/"
            "/^.+$/"
        );

        $options["author"]["email"] = $this->askDefaultRequiredMatch(
            "Author Email      ", $options["author"]["email"],
            "/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$/",
            "Author Email must be a valid email address!"
        );


        if($composerPath = realpath($this->path(__DIR__, "composer.json")))
        {
            $composer = file_get_contents($composerPath);

            $this->replaceJsonString($composer,
                "name", "mvqn/ucrm-plugin-template", $options['plugin']['vendor']."/".$options['plugin']['project']
            );

            $this->replaceJsonString($composer,
                "type", "template", "ucrm-plugin"
            );

            $this->replaceJsonStringArray($composer,
                "keywords", [ "ucrm", "unms", "plugins", "template" ], [ "ucrm", "unms", "plugins" ]
            );

            $this->replaceJsonString($composer,
                "description", "A project template for developing UCRM Plugins.", $options["plugin"]["description"]
            );

            $this->replaceJsonString($composer,
                "license", "MIT", $options["plugin"]["license"]
            );

            $this->replaceJsonString($composer,
                "homepage", "https://gitlab.com/mvqn/ucrm-plugin-template", $options["plugin"]["url"]
            );

            $this->replaceJsonString($composer,
                "name", "Ryan Spaeth", $options["author"]["name"]
            );

            $this->replaceJsonString($composer,
                "email", "rspaeth@mvqn.net", $options["author"]["email"]
            );

            // TODO: Uncomment after testing!
            //file_put_contents($composerPath, $composer, LOCK_EX);
        }


        if($manifestPath = realpath($this->path(__DIR__, "src", "manifest.json")))
        {
            $manifest = file_get_contents($manifestPath);

            $this->replaceJsonString($manifest,
                "name", "ucrm-plugin-template", $options["plugin"]["project"]
            );

            $this->replaceJsonString($manifest,
                "displayName", "Plugin Template", $options["plugin"]["name"]
            );

            $this->replaceJsonString($manifest,
                "description", "A project template for developing UCRM Plugins.", $options["plugin"]["description"]
            );

            $this->replaceJsonString($manifest,
                "url", "https://gitlab.com/mvqn/ucrm-plugin-template", $options["plugin"]["url"]
            );

            $this->replaceJsonString($manifest,
                "version", "1.0.0", $options["plugin"]["version"]
            );

            $this->replaceJsonString($manifest,
                "author", "Ryan Spaeth", $options["author"]["name"]
            );

            // TODO: Uncomment after testing!
            //file_put_contents($manifestPath, $manifest, LOCK_EX);
        }

        if($packagePath = realpath($this->path(__DIR__, "src", "client", "package.json")))
        {
            $package = file_get_contents($packagePath);

            $this->replaceJsonString($package,
                "name", "ucrm-plugin-template", $options["plugin"]["project"]
            );

            $this->replaceJsonString($package,
                "version", "1.0.0", $options["plugin"]["version"]
            );

            $this->replaceJsonString($package,
                "name", "Ryan Spaeth", $options["author"]["name"]
            );

            $this->replaceJsonString($package,
                "email", "rspaeth@mvqn.net", $options["author"]["email"]
            );

            // TODO: Uncomment after testing!
            file_put_contents($packagePath.".json", $package, LOCK_EX);
        }





        $this->io()->newLine();



        $this->io()->title("Hooks");
        foreach($options["hooks"] as $hook => $value)
        {
            $options["hooks"][$hook] = strtoupper($this->askDefaultRequiredMatch(
                "Include ".str_pad("'hook_$hook.php'", 20), $value ? "Y" : "N", "/[YyNn]/"
            )) === "Y";

            if(!$options["hooks"][$hook])
            {
                if($path = realpath($this->path(__DIR__, "src", "hook_$hook.php")))
                {
                    // TODO: Uncomment after testing!
                    //unlink($path);
                    $this->say("Removed 'hook_$hook.php'");
                }
            }
        }








    }

    #endregion




    #region ENVIRONMENT

    /**
     * @param string $contents
     * @param array $match
     * @param string $key
     * @param string $value
     * @return string
     */
    private static function setEnvLine(string $contents, array $match, string $key, string $value): string
    {
        $contents = substr_replace($contents, $value, $match["value"]["start"], $match["value"]["length"]);
        $contents = substr_replace($contents, $key, $match["key"]["start"], $match["key"]["length"]);
        return $contents;
    }

    /**
     * @param string $contents
     * @param string $key
     * @param array $match
     * @return string|false
     */
    private static function getEnvLine(string $contents, string $key, array &$match = null)
    {
        // IF any RegEx match of KEY=[VALUE] exists in the string...
        if (preg_match_all("/^[ \t]*($key)[ \t]*=[ \t]*(.*)$/m", $contents, $matches, PREG_SET_ORDER | PREG_OFFSET_CAPTURE))
        {
            // NOTE: In the case of multiple duplicate entries, only get the value of the final entry...
            list($full, $key, $value) = array_pop($matches);

            // Set all the match meta-data.
            $match = [
                "key"   => [
                    "start"     => $key[1],
                    "length"    => strlen($key[0]),
                    "text"      => $key[0],
                ],
                "value" => [
                    "start"     => $value[1],
                    "length"    => strlen($value[0]),
                    "text"      => $value[0],
                ]
            ];

            // And return the assigned value!
            return $value[0];
        }

        // OTHERWISE, return false!
        return false;
    }

    #endregion

    #region Configuration

    /**
     * @return array
     */
    public function sftpConfigure(): array
    {
        // Initialize the configuration values.
        $host = "";
        $port = "";
        $user = "";
        $pass = "";

        // Initialize the existing content values.
        $contents = "";
        $matches = null;

        // IF an .env file exists...
        if($path = realpath(self::ENV_FILE_PATH))
        {
            // ...THEN, get it's contents.
            $contents = file_get_contents($path);

            // NOTE: Currently, commented lines do NOT match and should likely be left this way!

            // Get match information for any existing line items.
            $host = self::getEnvLine($contents, "SFTP_HOST", $hostMatch) ?: "";
            $port = self::getEnvLine($contents, "SFTP_PORT", $portMatch) ?: "";
            $user = self::getEnvLine($contents, "SFTP_USER", $userMatch) ?: "";
            $pass = self::getEnvLine($contents, "SFTP_PASS", $passMatch) ?: "";

            // Construct some match meta-data, based on the results...
            $matches[$hostMatch["key"]["start"] ?? -4] = $hostMatch
                ?? [ "key" => [ "text" => "SFTP_HOST" ], "value" => null ];
            $matches[$portMatch["key"]["start"] ?? -3] = $portMatch
                ?? [ "key" => [ "text" => "SFTP_PORT" ], "value" => null ];
            $matches[$userMatch["key"]["start"] ?? -2] = $userMatch
                ?? [ "key" => [ "text" => "SFTP_USER" ], "value" => null ];
            $matches[$passMatch["key"]["start"] ?? -1] = $passMatch
                ?? [ "key" => [ "text" => "SFTP_PASS" ], "value" => null ];

            // NOTE: here we sort the matches in descending order, as we need to alter the content values in the reverse
            // order to not change the starting positions when new values are not the same lengths as the originals.
            krsort($matches, );
        }

        // Prompt for configuration values based on any existing values found.
        //$host = $event->getIO()->ask("SFTP Postgres [$host]: ", $host);
        $host = $this->askDefault("SFTP Host", $host);
        //$port = $event->getIO()->ask("SFTP Port [$port]: ", $port);
        $port = $this->askDefault("SFTP Port", $port);
        //$user = $event->getIO()->ask("SFTP User [$user]: ", $user);
        $user = $this->askDefault("SFTP User", $user);
        //$pass = $event->getIO()->ask("SFTP Pass [$pass]: ", $pass);
        $pass = $this->askDefault("SFTP Pass", $pass);

        // Do some quoting fix-ups to make sure there are no issues with the password.
        $pass = trim($pass, "\"");
        $pass = "\"".$pass."\"";

        // Initialize an array to store any values that need to be appended as opposed to altered.
        $append = [];

        // IF matches were previously found...
        if($matches)
        {
            // ...THEN loop through each match...
            foreach ($matches as $index => $match)
            {
                // ...AND determine the variable name based on the text value.
                $name = strtolower(str_replace("SFTP_", "", $match["key"]["text"]));

                // IF a match exists, THEN update it, OTHERWISE append it!
                if($match["value"])
                    $contents = self::setEnvLine($contents, $match, $match["key"]["text"], $$name);
                else
                    $append[$index] = $match["key"]["text"]."=".$$name."\n";
            }
        }
        else
        {
            // Append ALL of the missing values, as none of them currently exists.
            $append[-4] = "SFTP_HOST=".$host."\n";
            $append[-3] = "SFTP_PORT=".$port."\n";
            $append[-2] = "SFTP_USER=".$user."\n";
            $append[-1] = "SFTP_PASS=".$pass."\n";
        }

        // IF any items need to be appended...
        if(!empty($append))
        {
            // ...THEN comment the contents
            $contents .= "\n";
            $contents .= "# Generated by PostCreateProject::configureSFTP()\n";

            // And append the configuration items.
            ksort($append);
            $contents .= implode("", $append);
        }

        // Finally, save the new contents to the original file or a new file as required.
        file_put_contents(self::ENV_FILE_PATH, $contents, LOCK_EX);

        return [
            "host" => $host,
            "port" => (int)$port,
            "user" => $user,
            "pass" => trim($pass, "\""),
        ];
    }

    #endregion


    #region SFTP





    /**
     * Prompts the developer for SFTP Configuration, saves or updates the results in a "sftp.config.json" file and then
     * adds the relative path to the ".gitignore" file.
     */
    /*
    public function sftpConfigure(): void
    {
        $this->askSftpConfiguration(__DIR__, "sftp.config.json");
    }
    */


    private function sftpLoadConfiguration(): array
    {
        // Set some initial placeholders for the configuration.
        $config = [
            "host" => "",
            "port" => 0,
            "user" => "",
            "pass" => "",
        ];

        // IF an ".env" file exists...
        if($path = realpath(self::ENV_FILE_PATH))
        {
            // ...THEN get it's contents.
            $contents = file_get_contents($path);

            // AND load any values that hae been provided.
            $config["host"] = $this->getEnvLine($contents, "SFTP_HOST") ?: "";
            $config["port"] = (int)$this->getEnvLine($contents, "SFTP_PORT") ?: 0;
            $config["user"] = $this->getEnvLine($contents, "SFTP_USER") ?: "";
            $config["pass"] = trim($this->getEnvLine($contents, "SFTP_PASS"), "\"") ?: "";
        }

        return $config;
    }

    /**
     * @param string $remote
     * @param string $local
     * @throws AuthenticationException
     * @throws InitializationException
     * @throws LocalStreamException
     * @throws MissingExtensionException
     * @throws OptionMissingException
     * @throws RemoteConnectionException
     * @throws RemoteStreamException
     */
    public function sftpGet(string $remote, string $local)
    {
        // Load any existing configuration.
        $config = $this->sftpLoadConfiguration();

        // Get the plugin's name.
        $plugin = $this->getPluginName();

        // Set the remote path, including the base path if a relative path is provided.
        $remote = strpos($remote, "/") === 0
            ? $remote
            : self::REMOTE_PLUGIN_PATH."/$plugin/$remote";

        // Set the local path, including the base path if a relative path is provided.
        $local  = strpos($remote, ":\\") !== false
            ? $local
            : __DIR__.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."$local";

        // Create the task.
        $this->taskSftpGet()

            // Set the configuration from above, even if it contains mostly defaults.
            ->setOptions($config)

            // Handle any configuration fix-ups.
            ->funcConfiguration(
                function(array $current) use ($remote, $local)
                {
                    // IF any the necessary information is missing, prompt the user for input...
                    if ($current["host"] === "" || $current["port"] === 0 ||
                        $current["user"] === "" || $current["pass"] === "")
                        $current = $this->sftpConfigure();

                    // AND return the fixed configuration.
                    return $current;
                }
            )

            // Map the remote file to the local file.
            ->map($remote, $local)

            // And run the task!
            ->run();
    }

    /**
     * @param string $local
     * @param string $remote
     *
     * @throws AuthenticationException
     * @throws InitializationException
     * @throws LocalStreamException
     * @throws MissingExtensionException
     * @throws OptionMissingException
     * @throws RemoteConnectionException
     * @throws RemoteStreamException
     */
    public function sftpPut(string $local, string $remote)
    {
        // Load any existing configuration.
        $config = $this->sftpLoadConfiguration();

        // Get the plugin's name.
        $plugin = $this->getPluginName();

        // Set the local path, including the base path if a relative path is provided.
        $local = strpos($remote, ":\\") !== false
            ? $local
            : __DIR__.DIRECTORY_SEPARATOR."src".DIRECTORY_SEPARATOR."$local";

        // Set the remote path, including the base path if a relative path is provided.
        $remote = strpos($remote, "/") === 0
            ? $remote
            : self::REMOTE_PLUGIN_PATH."/$plugin/$remote";

        // Create the task.
        $this->taskSftpPut()

            // Set the configuration from above, even if it contains mostly defaults.
            ->setOptions($config)

            // Handle any configuration fix-ups.
            ->funcConfiguration(
                function(array $current) use ($remote, $local)
                {
                    // IF any the necessary information is missing, prompt the user for input...
                    if ($current["host"] === "" || $current["port"] === 0 ||
                        $current["user"] === "" || $current["pass"] === "")
                        $current = $this->sftpConfigure();

                    // AND return the fixed configuration.
                    return $current;
                }
            )

            // Map the local file to the remote file.
            ->map($local, $remote)

            // TODO: Change file permissions???

            // And run the task!
            ->run();
    }

    #endregion







}



