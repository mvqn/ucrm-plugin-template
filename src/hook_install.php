<?php
declare(strict_types=1);

#region DEFAULTS: data/config.json

echo "Setting default configuration in 'data/config.json'...";

// Configure any templates to be populated in data/config.json after Plugin installation.
$config = [
    "development"   => true,
    // ...
];

// Save the default settings.
file_put_contents(__DIR__ . "/data/config.json", json_encode($config));

echo "DONE!\n";

#endregion

#region DEFAULTS: data/permissions.json

echo "Setting default permissions in 'data/permissions.json'...";

$permissions = [
    "groups" => [
        "Admin Group",
    ],
    "users" =>  [],
];

file_put_contents(__DIR__ . "/data/permissions.json", json_encode($permissions));

echo "DONE!\n";

#endregion

#region DATABASE

echo "Creating 'logs' table in 'data/plugin.db'...";

$dbPath             = __DIR__."/data/plugin.db";
$pluginDatabase     = new PDO("sqlite:".$dbPath);

// Create the "logs" table, if it does not exist...
$pluginDatabase->exec(<<<SQL
    CREATE TABLE IF NOT EXISTS logs
    (
        id          INTEGER PRIMARY KEY,
        timestamp   TEXT,
        channel     TEXT    NOT NULL,
        level       INTEGER NOT NULL,
        level_name  TEXT    NOT NULL,
        message     TEXT,
        context     TEXT,
        extra       TEXT                
    );
    SQL
);

echo "DONE!\n";

#endregion


