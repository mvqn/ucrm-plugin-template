<?php
declare(strict_types=1);

define(
    "PLUGIN_PATH",
    realpath(__DIR__)
);

define(
    "PLUGIN_NAME",
    json_decode(@file_get_contents(PLUGIN_PATH."/manifest.json") ?: "", true)["information"]["name"]
    ?? basename(dirname(__DIR__, 1))
);


define("PLUGIN_LOG_PATH",   realpath(PLUGIN_PATH."/data/plugin.log"));
define("PLUGIN_DB_PATH",    realpath(PLUGIN_PATH."/data/plugin.db"));

/*
function plugin_log(string $message)
{

}
*/
