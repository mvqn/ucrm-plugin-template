<?php
declare(strict_types=1);





#region LOGS

$logPath            = __DIR__."/data/plugin.log";
file_put_contents($logPath, "Log file cleared during plugin update!", LOCK_EX);

#endregion

#region DATABASE



$dbPath             = __DIR__."/data/plugin.db";
$pluginDatabase     = new PDO("sqlite:".$dbPath);

// Empty the logs table, as we do not want to keep entries from previous versions...
/** @noinspection SqlWithoutWhere */
$pluginDatabase->exec(<<<SQL
    DELETE FROM logs;
    SQL
);

// Clean the database file to reduce disk space...
$pluginDatabase->exec(<<<SQL
    VACUUM;
    SQL
);

file_put_contents($logPath, "Log table cleared during plugin update!", LOCK_EX);

#endregion
