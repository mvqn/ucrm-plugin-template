<?php
/** @noinspection PhpUnusedParameterInspection */
/** @noinspection PhpUnusedLocalVariableInspection */
declare(strict_types=1);

namespace App\Controllers\API;

use App\Formatters\SqlFormatter;
use App\Settings;
use MVQN\Data\Database;
use MVQN\Data\Exceptions\DatabaseConnectionException;
use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;



/**
 * Class PgsqlController
 *
 * An controller for interacting with the Plugin's log files.
 *
 * @package App\Controllers
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class PgsqlController
{
    /**
     * @throws DatabaseConnectionException
     */
    private static function connect()
    {
        /** @noinspection PhpUndefinedClassConstantInspection */
        Database::connect(
            getenv("POSTGRES_HOST") ?: Settings::UCRM_DB_HOST,
            (int)getenv("POSTGRES_PORT") ?: Settings::UCRM_DB_PORT,
            getenv("POSTGRES_DB") ?: Settings::UCRM_DB_NAME,
            getenv("POSTGRES_USER") ?: Settings::UCRM_DB_USER,
            getenv("POSTGRES_PASSWORD") ?: Settings::UCRM_DB_PASSWORD
        );
    }

    /**
     * Gets a list of all tables in the database.
     *
     * @param bool $schema When TRUE, prefix table names with the discovered schema. Defaults to FALSE.
     * @return array Returns an array of table names found in the database.
     * @throws DatabaseConnectionException
     */
    private static function getTables(bool $schema = false) : array
    {
        // Connect to the database!
        self::connect();

        // Query the database for a list of tables.
        $results = Database::query(<<<SQL
            SELECT * FROM pg_catalog.pg_tables
            WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema';
            SQL
        );

        // Reduce the list to only table names, including the schema prefix when specified.
        $results = array_map(
            function($row) use ($schema)
            {
                /** @noinspection SpellCheckingInspection */
                return ($schema ? $row["schemaname"] . "." : "") . $row["tablename"];
            },
            $results
        );

        // Sort the results, alphabetically.
        sort($results);

        // And return the list!
        return $results;
    }

    /**
     * @param string $schema
     * @param string $table
     * @param bool $meta
     *
     * @return array
     * @throws DatabaseConnectionException
     */
    private static function getColumns(string $schema, string $table, bool $meta = false) : array
    {
        // Connect to the database!
        self::connect();

        /** @noinspection SqlResolve */
        $results = Database::query(<<<SQL
            SELECT * FROM information_schema.columns
            WHERE table_schema = '$schema' AND table_name = '$table';
            SQL
        );


        $columns = array_map(
            function($row) use ($meta)
            {
                if(!$meta)
                    return $row["column_name"];

                switch($row["udt_name"])
                {
                    case "uuid":        $type = "uuid";         break;
                    case "bool":        $type = "bool";         break;
                    case "int2":        $type = "short";        break;
                    case "int4":        $type = "int";          break;
                    case "int8":        $type = "long";         break;
                    case "float4":      $type = "float";        break;
                    case "float8":      $type = "double";       break;
                    case "text":
                    case "varchar":     $type = "string";       break;
                    case "timestamp":
                    /** @noinspection SpellCheckingInspection */
                    case "timestamptz": $type = "timestamp";    break;
                    case "json":        $type = "json";         break;
                    case "date":        $type = "date";         break;

                    default:            $type = "?"; //$row["udt_name"];
                }

                return [
                    "name" => $row["column_name"],
                    "nullable" => ($row["is_nullable"] === "YES"),
                    "type" => $type,
                ];
            },
            $results
        );

        return $columns;
    }

    /**
     * PgsqlController constructor.
     *
     * @param App $app The Slim Application for which to configure routing.
     */
    public function __construct(App $app)
    {
        // Get a local reference to the Slim Application's DI Container.
        $container = $app->getContainer();

        #region GET /pgsql/tables/[{schema=ucrm}.]{table}

        // Handle GET queries to the file logs...
        $app->get(
            "/pgsql/tables/{schema:(?:\w+\.)*}{table}",

            function (Request $request, Response $response, array $args) use ($container)
            {
                // Parse the requested schema name from the URL, or default to "ucrm" if none was provided.
                $schema = array_key_exists("schema", $args) && $args["schema"] !== ""
                    ? rtrim($args["schema"], ".")
                    : "ucrm";

                // Parse the requested table nam e from the URL.
                $table  = $args["table"];

                return $response->withJson(self::getColumns($schema, $table, true));
            }
        );

        #endregion

        #region GET /pgsql/tables

        // Handle GET queries to the file logs...
        $app->get(
            "/pgsql/tables[/]",

            function (Request $request, Response $response, array $args) use ($container)
            {
                return $response->withJson(self::getTables());
            }
        );

        #endregion

        #region GET /pgsql/schemas

        $app->get(
            "/pgsql/schemas[/]",

            function (Request $request, Response $response, array $args) use ($container)
            {
                // Connect to the database.
                self::connect();

                // Query the database for the schema metadata in which we are interested.
                $results = Database::query(<<<SQL
                    SELECT table_name, column_name, udt_name, is_nullable::bool, ordinal_position
                    FROM INFORMATION_SCHEMA.COLUMNS
                    WHERE table_name IN (
                        SELECT tablename
                        FROM pg_catalog.pg_tables
                        WHERE schemaname != 'pg_catalog' AND schemaname != 'information_schema'
                    );
                    SQL
                );

                // Sort the results.
                array_multisort(
                    array_column($results, "table_name"),  SORT_ASC,
                    //array_column($results, "column_name"), SORT_ASC,
                    array_column($results, "ordinal_position"), SORT_ASC,
                    $results
                );

                // And return the results formatted as JSON.
                return $response->withJson($results);
            }
        );

        #endregion

        #region POST /pgsql/query

        $app->post(
            "/pgsql/query[/]",

            function (Request $request, Response $response, array $args) use ($container)
            {
                $body = $request->getBody()->getContents();

                // TODO: Here we should "sanitize" the request to prevent writeable queries!!!

                self::connect();

                $results = Database::query($body);

                return $response->withJson($results);
            }
        );

        #endregion

        #region POST /pgsql/format

        $app->post(
            "/pgsql/format[/]",

            function (Request $request, Response $response, array $args) use ($container)
            {
                $body = $request->getBody()->getContents();
                $formatted = SqlFormatter::format($body);
                return $response->withHeader("Content-Type", "text/html")->write($formatted);
            }
        );

        #endregion



        #region GET /pgsql/user-groups

        $app->get(
            "/pgsql/user-groups[/]",

            function (Request $request, Response $response, array $args) use ($container)
            {
                // Connect to the database!
                self::connect();

                // Query the database for all defined user-groups.
                $results = Database::query(<<<SQL
                    SELECT * FROM ucrm.user_group;
                    SQL
                );

                return $response->withJson($results);
            }
        );

        #endregion

    }

}
