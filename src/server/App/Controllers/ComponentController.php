<?php
declare(strict_types=1);

namespace App\Controllers;

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;
use Slim\Views\Twig;

/**
 * Class ComponentController
 *
 * A controller to handle component delivery to the configuration extension.
 *
 * @package App\Controllers
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 * @final
 */
final class ComponentController
{
    /**
     * ComponentController constructor.
     *
     * @param App $app The Slim Application for which to configure routing.
     */
    public function __construct(App $app)
    {
        $app->get("/component/{name}",

            function (Request $request, Response $response, array $args)
            {
                /** @var Twig $twig */
                $twig = $this["twig"];

                $file = realpath(__DIR__."/../Views/components/${args['name']}.html.twig");

                return $file
                    ? $twig->render($response, "components/${args['name']}.html.twig", $request->getQueryParams())
                    : $response->write("Not Found!")->withStatus(404);
            }
        );
    }

}
