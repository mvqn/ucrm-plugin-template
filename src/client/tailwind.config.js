"use strict";

/**
 * TailwindCSS Configuration
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */

// Add any Tailwind CSS configuration here...
module.exports = {

    theme: {
        extend: {

        }
    },

    variants: {

    },

    plugins: [

    ]

};
