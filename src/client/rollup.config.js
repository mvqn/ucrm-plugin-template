"use strict";

/**
 * RollupJS Configuration
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */

import uglifyES from "rollup-plugin-uglify-es";
import importCSS from "@atomico/rollup-plugin-import-css";
import importJSON from "@rollup/plugin-json";
import includePaths from "rollup-plugin-includepaths";

// noinspection JSUnusedGlobalSymbols,SpellCheckingInspection
const common =
{
    plugins: [
        uglifyES({
            mangle: false,
        }),
        importCSS(),
        importJSON(),
        includePaths({
            paths: [ "./admin-zone/", ".client-zone" ],
            extensions: [ ".js", ".json", ".css" ]
        })
    ],

    onwarn: function(warning, next)
    {
        // Ignore irrelevant warnings...
        if (warning.code === "DEPRECATED_FEATURE" || warning.code === "EMPTY_BUNDLE")
            return;

        next(warning);
    }

};

// noinspection JSUnusedGlobalSymbols,SpellCheckingInspection
export default
[
    // Handle compilation of stand-alone "admin-zone.js"...
    {
        input: "./admin-zone/admin-zone.js",
        output: {
            file: "../public/admin-zone.js",
            format: "iife",
            //sourcemap: true,
        },
        ...common,
    },

    // Handle compilation of stand-alone "client-zone.js"...
    {
        input: "./client-zone/client-zone.js",
        output: {
            file: "../public/client-zone.js",
            format: "iife",
            //sourcemap: true,
        },
        ...common,
    },
]
