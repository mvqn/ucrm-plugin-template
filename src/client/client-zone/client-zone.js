"use strict";

/**
 * Client-Zone JS
 *
 * This file is handled by RollupJS, which supports the following here:
 * - Converts `import` calls to inline code, including JS, CSS and JSON.
 * - Transpiles ES6 code to an IIFE for browser execution.
 * - Uses UglifyES on the transpiled code, but does not mangle names.
 *
 * Many other features supported by RollupJS can be added via plugins in the `../rollup.config.js` file.
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */

