"use strict";

/**
 * Babel Configuration
 *
 * @author Ryan Spaeth <rspaeth@mvqn.net>
 */

module.exports = {
    presets: [
        '@vue/app'
    ]
};
